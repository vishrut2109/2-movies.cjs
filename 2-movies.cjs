const favouritesMovies = {
    "Matrix": {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M"
    },
    "FightClub": {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M"
    },
    "Inception": {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M"
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M"
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M"
    },
    "Titanic": {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M"
    }
}


/*
    NOTE: For all questions, the returned data must contain all the movie information including its name.

    Q1. Find all the movies with total earnings more than $500M. 
    Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
    Q.3 Find all movies of the actor "Leonardo Dicaprio".
    Q.4 Sort movies (based on IMDB rating)
        if IMDB ratings are same, compare totalEarning as the secondary metric.
    Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
        drama > sci-fi > adventure > thriller > crime

    NOTE: Do not change the name of this file
*/ 

//     Q1. Find all the movies with total earnings more than $500M. 
const moviesArray1=Object.entries(favouritesMovies)
const moviesEarningsGreater500M=moviesArray1.filter((movie)=>{
    return Number( movie[1].totalEarnings.replace(/\$|\M/g,""))>500
})
console.log(moviesEarningsGreater500M)


//    Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
const moviesArray2=Object.entries(favouritesMovies)
const moviesEarningsGreater500MAndOscarNominationsGreater3=moviesArray2.filter((movie)=>{
    return Number( movie[1].totalEarnings.replace(/\$|\M/g,""))>500 && movie[1].oscarNominations>3
})
console.log(moviesEarningsGreater500MAndOscarNominationsGreater3)

// Q.3 Find all movies of the actor "Leonardo Dicaprio"
const moviesArray3=Object.entries(favouritesMovies)
const moviesLeonardo=moviesArray3.filter((movie)=>{
    const actors=movie[1].actors
    return actors.includes("Leonardo Dicaprio")
})
console.log(moviesLeonardo)

// Q.4 Sort movies (based on IMDB rating)
// if IMDB ratings are same, compare totalEarning as the secondary metric.

const moviesArray4=Object.entries(favouritesMovies)
const moviesSorted=moviesArray4.sort((currentMovie,nextMovie)=>{
    currentMovieEarning= Number(currentMovie[1].totalEarnings.replace(/\$|\M/g,""))
    nextMovieEarning= Number(nextMovie[1].totalEarnings.replace(/\$|\M/g,""))
    if(currentMovie[1].imdbRating===nextMovie[1].imdbRating){
        return nextMovieEarning-currentMovieEarning
    }
    else{
        return nextMovie[1].imdbRating-currentMovie[1].imdbRating
    }
})
console.log(moviesSorted)

//     Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
// drama > sci-fi > adventure > thriller > crime

const moviesArray5=Object.entries(favouritesMovies)
const moviesGrouped=moviesArray5.map((movie)=>{
    if(movie[1].genre.includes("drama")){
        movie[1]["group"]="drama"
        return movie
    }
    else if(movie[1].genre.includes("sci-fi")){
        movie[1]["group"]="sci-fi"
        return movie
    }
    else if(movie[1].genre.includes("adventure")){
        movie[1]["group"]="adventure"
        return movie

    }
    else if(movie[1].genre.includes("thriller")){
        movie[1]["group"]="thriller"
        return movie

    }
    else if(movie[1].genre.includes("crime")){
        movie[1]["group"]="crime"
        return movie
    }
}).reduce((groupMovies,movie)=>{
    const group=movie[1].group
    if(groupMovies[group]){
        groupMovies[group].push(movie)
    }else{
        groupMovies[group]=[]
        groupMovies[group].push(movie)
    }
    return groupMovies
},{})
console.log(moviesGrouped)
